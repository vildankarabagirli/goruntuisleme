﻿namespace TMH306
{
    partial class griFr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.griYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ortalamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.islemBox = new System.Windows.Forms.PictureBox();
            this.kaynakBox = new System.Windows.Forms.PictureBox();
            this.lUMAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCIKLIKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEKRENKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEKRENKYEŞİLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEKRENKMAVİToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.griYöntemleriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1404, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(109, 26);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // griYöntemleriToolStripMenuItem
            // 
            this.griYöntemleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ortalamaToolStripMenuItem,
            this.bT709ToolStripMenuItem,
            this.lUMAToolStripMenuItem,
            this.aCIKLIKToolStripMenuItem,
            this.tEKRENKToolStripMenuItem,
            this.tEKRENKYEŞİLToolStripMenuItem,
            this.tEKRENKMAVİToolStripMenuItem});
            this.griYöntemleriToolStripMenuItem.Name = "griYöntemleriToolStripMenuItem";
            this.griYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(117, 24);
            this.griYöntemleriToolStripMenuItem.Text = "Gri Yöntemleri";
            // 
            // ortalamaToolStripMenuItem
            // 
            this.ortalamaToolStripMenuItem.Name = "ortalamaToolStripMenuItem";
            this.ortalamaToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.ortalamaToolStripMenuItem.Text = "Ortalama";
            this.ortalamaToolStripMenuItem.Click += new System.EventHandler(this.ortalamaToolStripMenuItem_Click);
            // 
            // bT709ToolStripMenuItem
            // 
            this.bT709ToolStripMenuItem.Name = "bT709ToolStripMenuItem";
            this.bT709ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.bT709ToolStripMenuItem.Text = "BT-709";
            this.bT709ToolStripMenuItem.Click += new System.EventHandler(this.bT709ToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // islemBox
            // 
            this.islemBox.Location = new System.Drawing.Point(764, 54);
            this.islemBox.Margin = new System.Windows.Forms.Padding(4);
            this.islemBox.Name = "islemBox";
            this.islemBox.Size = new System.Drawing.Size(613, 591);
            this.islemBox.TabIndex = 5;
            this.islemBox.TabStop = false;
            // 
            // kaynakBox
            // 
            this.kaynakBox.Location = new System.Drawing.Point(27, 54);
            this.kaynakBox.Margin = new System.Windows.Forms.Padding(4);
            this.kaynakBox.Name = "kaynakBox";
            this.kaynakBox.Size = new System.Drawing.Size(613, 591);
            this.kaynakBox.TabIndex = 4;
            this.kaynakBox.TabStop = false;
            // 
            // lUMAToolStripMenuItem
            // 
            this.lUMAToolStripMenuItem.Name = "lUMAToolStripMenuItem";
            this.lUMAToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.lUMAToolStripMenuItem.Text = "LUMA";
            this.lUMAToolStripMenuItem.Click += new System.EventHandler(this.lUMAToolStripMenuItem_Click);
            // 
            // aCIKLIKToolStripMenuItem
            // 
            this.aCIKLIKToolStripMenuItem.Name = "aCIKLIKToolStripMenuItem";
            this.aCIKLIKToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.aCIKLIKToolStripMenuItem.Text = "ACIKLIK";
            this.aCIKLIKToolStripMenuItem.Click += new System.EventHandler(this.aCIKLIKToolStripMenuItem_Click);
            // 
            // tEKRENKToolStripMenuItem
            // 
            this.tEKRENKToolStripMenuItem.Name = "tEKRENKToolStripMenuItem";
            this.tEKRENKToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.tEKRENKToolStripMenuItem.Text = "TEK RENK";
            this.tEKRENKToolStripMenuItem.Click += new System.EventHandler(this.tEKRENKToolStripMenuItem_Click);
            // 
            // tEKRENKYEŞİLToolStripMenuItem
            // 
            this.tEKRENKYEŞİLToolStripMenuItem.Name = "tEKRENKYEŞİLToolStripMenuItem";
            this.tEKRENKYEŞİLToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.tEKRENKYEŞİLToolStripMenuItem.Text = "TEK RENK YEŞİL";
            this.tEKRENKYEŞİLToolStripMenuItem.Click += new System.EventHandler(this.tEKRENKYEŞİLToolStripMenuItem_Click);
            // 
            // tEKRENKMAVİToolStripMenuItem
            // 
            this.tEKRENKMAVİToolStripMenuItem.Name = "tEKRENKMAVİToolStripMenuItem";
            this.tEKRENKMAVİToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.tEKRENKMAVİToolStripMenuItem.Text = "TEK RENK MAVİ";
            this.tEKRENKMAVİToolStripMenuItem.Click += new System.EventHandler(this.tEKRENKMAVİToolStripMenuItem_Click);
            // 
            // griFr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1404, 699);
            this.Controls.Add(this.islemBox);
            this.Controls.Add(this.kaynakBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "griFr";
            this.Text = "griFr";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem griYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ortalamaToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox islemBox;
        private System.Windows.Forms.PictureBox kaynakBox;
        private System.Windows.Forms.ToolStripMenuItem bT709ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lUMAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCIKLIKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tEKRENKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tEKRENKYEŞİLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tEKRENKMAVİToolStripMenuItem;
    }
}